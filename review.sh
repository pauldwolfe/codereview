#!/usr/bin/env bash

display_useage() {
    echo "Usage: $0 (start|pull) [jira]" >&2
    echo
    echo "   start           Start a new review. Creates a new branch for which you can create a pull request"
    echo "   pull            Pull from master. No JIRA required"
    echo "   jira            Mandatory when starting a review. The name to the JIRA for which this will be committed"
    exit 1
}

start_review() {
    if [ $# -eq 0 ]
    then
        echo "You must specify a JIRA to start a review. ATLS-XX"
        exit 1
    fi
    git checkout master
    git pull
    git branch | grep -v "master" | xargs git branch -D
    git checkout -b $1
}

pull_from_master() {
    git stash
    git pull --rebase origin master
    git stash pop
}

if [ $# -eq 0 ]
then
    display_useage
fi
subcommand=$1
shift
case "$subcommand" in
  start)
    jira=$1
    start_review $jira
    shift
    ;;
  pull)
    pull_from_master
esac